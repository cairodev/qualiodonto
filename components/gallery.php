<button data-modal-target="medium-modal" data-modal-toggle="medium-modal" class="" type="button"></button>
<section class="overflow-hidden text-gray-700 pb-4">
	<div class="container px-5 py-2 mx-auto lg:pt-4 lg:px-32">
		<h1 class="lg:text-3xl text-lg text-center mb-4 uppercase"><span class="font-normal text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">NOSSA ESTRUTURA</span></h1>
		<div class="flex flex-wrap -m-1 md:-m-2">
			<?php
			$stmt = $DB_con->prepare("SELECT * FROM estructure order by id desc");
			$stmt->execute();
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
			?>
				<!-- Modal toggle -->
				<button data-modal-target="id<?php echo $id ?>" data-modal-toggle="id<?php echo $id ?>" class="flex flex-wrap md:w-1/3 objectL hover:scale-105 transition duration-300" type="button">
					<div class="w-full px-8 p-2 md:p-3">
						<img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg" src="<?php echo $URI->base("/admin/uploads/estructure/$img"); ?>">
					</div>
				</button>

				<!-- Main modal -->
				<div id="id<?php echo $id ?>" data-modal-backdrop="static" tabindex="-1" aria-hidden="true" class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full">
					<div class="relative w-full h-full max-w-4xl md:h-auto">
						<!-- Modal content -->
						<div class="relative bg-white rounded-lg shadow">
							<!-- Modal header -->
							<div class="flex items-start justify-between p-4 border-b rounded-t">
								<h3 class="text-xl font-semibold text-gray-900">
									<?php echo $description ?>
								</h3>
								<button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center" data-modal-hide="id<?php echo $id ?>">
									<svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
										<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
									</svg>
								</button>
							</div>
							<!-- Modal body -->
							<div class="p-6 space-y-6">
								<img alt="gallery" class="block object-cover object-center w-full h-full rounded-lg" src="<?php echo $URI->base("/admin/uploads/estructure/$img"); ?>">
							</div>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>
		<div class="flex justify-center mt-4"><button id="btnLimit" type="submit" onclick="verMaisMenos()" class="w-48 px-3 py-4 rounded-md form-button bg-color1 text-white">Ver mais</button></div>

		<div class="mt-8"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3974.2488108505445!2d-42.79858398465681!3d-5.0633723528353975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x78e3a27cc3287a3%3A0x8af1c303c8eb0fd7!2sQualiodonto!5e0!3m2!1spt-BR!2sbr!4v1676582370848!5m2!1spt-BR!2sbr" width="100%" height="250" frameborder="0" style="border-radius:20px; border: 2px solid #006CAC ;" allowfullscreen=""></iframe></div>
		<script>
			let limit = true
			let objects = document.getElementsByClassName("objectL");
			for (i = 3; i < objects.length; i++) {
				objects[i].style.display = 'none'
			}

			function verMaisMenos() {
				if (limit == true) {
					for (i = 4; i < objects.length; i++) {
						objects[i].style.display = 'block'
					}
					document.getElementById("btnLimit").textContent = "Ver menos"
					limit = false
				} else if (limit == false) {
					for (i = 4; i < objects.length; i++) {
						objects[i].style.display = 'none'
					}
					document.getElementById("btnLimit").textContent = "Ver mais"
					limit = true
				}
			}
		</script>

	</div>
</section>