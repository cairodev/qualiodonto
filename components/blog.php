<section class="swiper swiper_blog pt-10 bg-color1">
  <div class="justify-center flex space-x-4 items-center p-10">
    <h1 class="lg:text-3xl text-lg text-center mb-4 uppercase"><span class="font-normal text-white">BLOG</span></h1>
    <!-- <img src="<?php // echo $URI->base("/assets/img/logo.png"); 
                    ?>" class="w-36 logo" alt="<?php echo $title; ?>" /> -->
  </div>
  <div class="swiper-wrapper max-w-screen-xl mx-auto">
    <div class="swiper-slide">
      <div class="max-w-lg bg-white p-6 mx-auto rounded-lg shadow_csc bg-white">
        <div class="rounded-2xl">
          <h3 style="color:#006CAC !important" class="mb-4 text-lg font-black py-2">QUAIS AS INDICAÇÕES DO CLAREAMENTO DENTAL ?</h3>
        </div>
        <div>
          <img class="rounded-md h-62 w-full" src="./admin/uploads/blog/blog1.jpg">
        </div>
        <div style="color:black !important" class="product_info">
          Antes de realizar o clareamento dental, aqui na QUALIODONTO o paciente passa por uma avaliação bucal minuciosa e antes do clareamento é feito toda a adequação do meio bucal
        </div>
      </div>
    </div>
    <div class="swiper-slide ">
      <div class="max-w-lg p-6 mx-auto rounded-lg shadow_csc bg-white">
        <div class="rounded-2xl">
          <h3 style="color:#006CAC !important" class="mb-4 text-lg font-black py-2">ENTENDA PORQUE É TÃO IMPORTANTE SUBSTITUIR UM DENTE PERDIDO</h3>
        </div>
        <div>
          <img class="rounded-md h-62 w-full" src="./admin/uploads/blog/blo2.jpg">
        </div>
        <div style="color:black !important" class="product_info">
          Dentes vizinhos movimentam-se na tentativa de preencher o espaço inexistente. A consequência é o desalinhamento dos dentes que interferem na mordida e na fala.
        </div>
      </div>
    </div>
  </div>
</section>