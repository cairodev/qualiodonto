<footer class="bg-white">
  <div class="max-w-6xl p-4 py-6 mx-auto lg:py-16 md:p-8 lg:p-10">
    <div class="grid grid-cols-1 gap-8 md:grid-cols-3 lg:grid-cols-4">
      <div>
        <div class="justify-center flex">
          <img src="<?php echo $URI->base('/assets/img/logo.png') ?>">
        </div>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-color1 uppercase">
          WWW.QUALIODONTO.COM.BR
        </h3>
        <ul class="text-color1">
          <li class="mb-4">
            <a href="#" class="hover:underline">Serviços</a>
          </li>
          <li class="mb-4">
            <a href="#" class="hover:underline">Quem Somos</a>
          </li>
          <!-- <li class="mb-4">
            <a href="#" class="hover:underline">Home</a>
          </li> -->
        </ul>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-color1 uppercase">Contato</h3>
        <p class="text-color1">
          Telefone: (86) 98149-8983
          Endereço: Av. Universitária, 750 - Sala 41 - Fátima, Teresina - PI, 64049-494
        </p>
      </div>
      <div>
        <a href="https://api.whatsapp.com/send?phone=5586981498983">
          <i class="text-color1 bi bi-whatsapp"></i>
        </a>
        <a href="https://www.instagram.com/qualiodontothe/">
          <i class="text-color1 bi bi-instagram"></i>
        </a>
        <a href="https://www.facebook.com/qualiodontothe">
          <i class="text-color1 bi bi-facebook"></i>
        </a>
      </div>
    </div>
    <div class="text-center pt-10">
      <span class="block text-base text-center text-color1">© 2023 - QUALI ODONTO
      </span>
      <div class="pt-10">
        <span class="block text-xs text-center text-color1">Site criado por
        </span>
        <span class="block text-xs text-center text-color1">Web Developer Full Stack: @cairofelipedev
        </span>
      </div>
    </div>
  </div>
</footer>

<script src="<?php echo $URI->base('/assets/js/dark_mode.js') ?>"></script>
<script src="https://unpkg.com/flowbite@1.4.1/dist/flowbite.js"></script>