<div class="mx-auto max-w-7xl pt-6 about" id="quem-somos">
<a href="<?php echo $URI->base("quem-somos"); ?>"><img class="hidden lg:block w-full" style="border-radius: 15px;" src="<?php echo $URI->base("/assets/img/quemsomos1.jpg"); ?>" /></a>
<a href="<?php echo $URI->base("quem-somos"); ?>"><img class="lg:hidden block w-full px-2" style="border-radius: 15px;" src="<?php echo $URI->base("/assets/img/quemsomosmob1.jpg"); ?>" /></a>
</div>