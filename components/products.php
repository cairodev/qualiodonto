<section class="swiper swiper_products products max-w-screen-xl mx-auto pt-10">
  <h1 class="lg:text-3xl text-lg text-center mb-4 uppercase"><span class="font-normal text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3"> Nossos Principais Procedimentos</span></h1>
  <div class="swiper-wrapper">
    <?php $stmt = $DB_con->prepare("SELECT * FROM products order by id desc");
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      extract($row);
    ?>
      <div class="swiper-slide ">
        <div class="<?php echo $bg; ?> max-w-lg bg-white p-6 mx-auto rounded-lg shadow_csc">
          <div class="rounded-2xl">
            <h3 style="color:#006CAC!important" class="text-center mb-4 text-xl font-normal py-2"><strong><?php echo $name; ?></strong></h3>
          </div>
          <div>
            <img style="object-fit: cover; object-position: top;" class="rounded-md h-72 w-full" src="./admin/uploads/products/<?php echo $img; ?>">
          </div>
          <?php if (($price_show == 1) and ($price_position == 1)) { ?>
            <div class="flex items-baseline justify-center my-8">
              <span class="mr-2 lg:text-5xl text-3xl text-white font-extrabold">R$ <?php echo $price; ?></span>
              <span class="text-white">/mês</span>
            </div>
          <?php } ?>
          <div style="color:black !important" class="product_info">
            <?php echo $info ?>
          </div>
          <?php if (($price_show == 1) and ($price_position == 1)) { ?>
            <div class="flex items-baseline justify-center my-8">
              <span class="mr-2 lg:text-5xl text-3xl text-white font-extrabold">R$ <?php echo $price; ?></span>
              <span class="text-white">/mês</span>
            </div>
          <?php } ?>
          <div class="flex justify-center">
            <a style="color:white !important" href="<?php echo $URI->base('/tratamento/' . slugify($name)); ?>" class="text-<?php echo $btn_color ?> bg-<?php echo $btn_bg ?> focus:ring-4 bg-sky-600 rounded-md font-bold text-xl px-5 py-2 text-center">Saiba mais</a>
          </div>
        </div>
      </div>
    <?php
    }
    ?>
  </div>
</section>