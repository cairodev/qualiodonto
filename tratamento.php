<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
require "config/functions.php";
$URI = new URI();

$url = explode("/", $_SERVER['REQUEST_URI']);
$get_url = $url[3];
$get_url_2 = "";

$stmt = $DB_con->prepare("SELECT * FROM products");
$stmt->execute();
if ($stmt->rowCount() > 0) {
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);
		$string1 = remove_symbols_accents(utf8_decode($get_url));
		$string2 = remove_symbols_accents(utf8_decode($name));
		if ($string1 == $string2) {
			$get_url_2 = $name;
		}
	}
}

$stmt = $DB_con->prepare("SELECT * FROM products where name='$get_url_2'");
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	extract($row);
}
?>

<!DOCTYPE html>
<html lang="pt-br">

<style>
#info h2{
	color: black !important;
}
</style>
<head>
	<?php include "heads.php"; ?>
</head>

<body>
	<?php include "components/navbar.php"; ?>
	<div class="mx-auto max-w-6xl pt-6 pb-6">
		<div style="border-top-left-radius: 20px; border-top-right-radius: 20px;" class="py-6 px-6 md:py-8 md:px-24 bg-white">
		<h3 style="color: black !important;" class="text-2xl text-center text-bold py-2"><strong class="text-color1"><?php echo $name; ?></strong></h3>	
		</div>
		<div class="flex justify-center bg-white"><img style="width:82%; height:550px; object-fit:cover; object-position:top; border-radius:20px" src="<?php echo $URI->base("/admin/uploads/products/$img"); ?>"></div>
		<div id="info" style="border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;" class="py-6 px-8 md:py-12 md:px-24 bg-white">
		
			<?php echo $info ?>
		</div>
	</div>
	<?php include "components/footer.php"; ?>

</body>

</html>